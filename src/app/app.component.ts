import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { environment } from '../environments/environment';
import { Message } from './message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Employee Directory';

  employee: Employee = new Employee();
  employeeList: Employee[];
  page: number = 0;
  messages = [];
  isUpdate: boolean = false;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get(`${environment.api}/employee`).subscribe((data: any) => {
      this.employeeList = data;
    })
  }

  insertEmployee() {
    this.http.post(`${environment.api}/employee`, this.employee).subscribe((emp: any) => {
      this.employee = new Employee();
      this.employeeList.unshift(emp);
    })
  }

  deleteEmployee(id: string) {
    this.http.delete(`${environment.api}/employee/${id}`).subscribe((data: any) => {      
      this.http.get(`${environment.api}/employee`).subscribe((emps: any) => {
        this.employeeList = emps;
        this.messages.push(
          {
            id: this.messages.length + 1,
            msg: `${data.name}  was removed from the directory!`,
            type: 'danger'
          }
        )
      })
    })
  }

  loadToUpdate(emp: Employee) {
    this.employee = emp;
    this.isUpdate = true;
  }

  updateEmployee() {
    this.http.patch(`${environment.api}/employee/${this.employee.id}`, this.employee).subscribe((data: any) => {      
      this.http.get(`${environment.api}/employee`).subscribe((emps: any) => {
        this.employeeList = emps;
        this.employee = new Employee();
        this.isUpdate = false
        this.messages.push(
          {
            id: this.messages.length + 1,
            msg: `${data.name}  was updated in the directory!`,
            type: 'info'
          }
        )
      })
    })
  }

  closeAlert(msg) {
    this.messages.splice(this.messages.indexOf(msg) ,1)
  }
}
