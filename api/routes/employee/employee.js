'use strict'

module.exports = (app, db) => {
    // Get a list of all employees
    app.get('/employee', (req, res) => {
        db.employees.findAll().then(employees => {
            res.status(200).json(employees)
        })
    })

    app.post('/employee', (req, res) => {
        const name = req.body.name
        const email = req.body.email
        const phone = req.body.phone

        db.employees.create({
            name: name,
            email: email,
            phone: phone
        }).then(emp => {
            res.status(200).json(emp);
        }).catch(err => res.status(500).json(err))
    })

    app.delete('/employee/:id', (req, res) => {
        const id = req.params.id
        db.employees.findOne({
            where: {
                id: id
            }
        }).then(emp => {
            db.employees.destroy({
                where: {
                    id: id
                }
            }).then(deleted => {
                res.status(200).json(emp)
            })
        })
    })

    app.patch('/employee/:id', (req, res) => {
        const id = req.params.id;
        const name = req.body.name;
        const email = req.body.email;
        const phone = req.body.phone;

        db.employees.find({
            where: {
                id: id
            }
        }).then(emp => {
            return emp.updateAttributes(
                {
                    name: name,
                    email: email,
                    phone: phone
                }
            )
        }).then(emp => {
            res.status(200).json(emp)
        })
    })
}