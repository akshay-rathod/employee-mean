'use strict'

const routes = [
    require('./employee/employee')
]

module.exports = function router(app, db) {
    return routes.forEach(route => {
        route(app, db)
    })
}