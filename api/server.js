'use strict'
const config = require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const db = require('./config/db')
const router = require('./routes/index')

const app = express()
app.use('*', cors())
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

db.sequelize.sync().then(_ => {
    console.info('Database connection established!')
    app.listen(process.env.PORT, _ => {
        console.info(`API server started on port: ${process.env.PORT}`)
    })
    const seed = require('./config/seed')(db)
})


app.get('/', (req, res) => {
    res.status(200).json(
        {
            msg: 'API is live!'
        }
    )
})

router(app, db)

process.on('SIGINT', _ => {
    console.info('Halting the API Server!')
    db.sequelize.close().then(_ => {
        process.exit(0)
    })
})