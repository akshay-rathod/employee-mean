'use strict'

const Sequelize = require('sequelize')
const sequelize = new Sequelize(process.env.DB, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST, 
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT, 
    define: {
        underscored: true
    }
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.employees = require('../models/employee')(sequelize, Sequelize)

module.exports = db