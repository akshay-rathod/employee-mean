'use strict'
const async = require('async')
const faker = require('faker')

module.exports = (db) => {
//     if (db.) {

//    }

   db.employees.findAll({
       attributes: [[ db.sequelize.fn('COUNT', db.sequelize.col('name')), 'emp_count' ]]
   }).then(emp => {
       if(emp[0].dataValues.emp_count < 5) {
           function createEmp(emp, cb) {
               db.employees.create(emp)
               .then(emp => cb(null, emp))
               .catch(err => cb(err, null))
           }
        
           async.parallel([
               function(cb) {
                   const card = faker.helpers.userCard()
                   createEmp({
                       name: card.name,
                       email: card.email,
                       phone: faker.phone.phoneNumberFormat()
                   }, cb)
               },
               function(cb) {
                   const card = faker.helpers.userCard()
                   createEmp({
                       name: card.name,
                       email: card.email,
                       phone: faker.phone.phoneNumberFormat()
                   }, cb)
               },
               function(cb) {
                   const card = faker.helpers.userCard()
                   createEmp({
                       name: card.name,
                       email: card.email,
                       phone: faker.phone.phoneNumberFormat()
                   }, cb)
               },
               function(cb) {
                   const card = faker.helpers.userCard()
                   createEmp({
                       name: card.name,
                       email: card.email,
                       phone: faker.phone.phoneNumberFormat()
                   }, cb)
               },
               function(cb) {
                   const card = faker.helpers.userCard()
                   createEmp({
                       name: card.name,
                       email: card.email,
                       phone: faker.phone.phoneNumberFormat()
                   }, cb)
               }
           ], err => {
               if (err) console.error('Error during seeding: ', err)
               else console.info('Finished seeding!')
           })
       } 
   })

}