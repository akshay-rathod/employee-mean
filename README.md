# Employee Directory
### Application is deployed @ [Employee Directory](https://dissepimental-magni.000webhostapp.com/)
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Configuration
* Run `npm install` inside the root directory to install the angular dependencies.
* Run `npm install` inside the api directory to install the dependencies for Express REST API.
* Edit the [.env](api/.env) file to configure resources and credentials required for the API.
    * `DB_DIALECT`: Default is set to _mysql_
    * `DB_HOST`: Default is set to _localhost_
    * `DB_PORT`: Default is set to _3306_
    * `DB`: Name of the database to be used. Default is set to _employeedb_
    * `DB_USER`: Default is set to _root_
    * `DB_PASS`: Default is set to blank
    * `PORT`: Port for the REST API. Default is set to _3000_

## Development server

* Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
* Run `npm start` or `nodemon` in [api](api/) directory to start the API server.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

> Optionally leverage [Nodemon](https://nodemon.io/) to boost the development speed.
